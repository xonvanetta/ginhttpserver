package ginhttpserver

import (
	"errors"
	"time"

	"github.com/sirupsen/logrus"
)

// Errors
var (
	ErrMissingPort            = errors.New("ginhttpserver: missing config port")
	ErrMissingShutdownTimeout = errors.New("ginhttpserver: missing config shutdown timeout")
)

// Config das config
type Config struct {
	Port            string
	ShutdownTimeout time.Duration
	//LogLevel is only used with NewDefault
	LogLevel logrus.Level
}

//DefaultConfig has sane defaults
func DefaultConfig() Config {
	c := Config{}
	c.Port = "8080"
	c.ShutdownTimeout = time.Minute
	c.LogLevel = logrus.WarnLevel
	return c
}

func (c *Config) validate() error {
	if c.Port == "" {
		return ErrMissingPort
	}
	if c.ShutdownTimeout == 0 {
		return ErrMissingShutdownTimeout
	}

	return nil
}
